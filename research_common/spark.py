import os
from research_common.gitlab import latest_package_file
from pyspark.sql import SparkSession
import subprocess

SPARK_HOME = "/usr/lib/spark3/"

default_spark_config = {
    "spark.dynamicAllocation.maxExecutors": 96,
    "spark.sql.shuffle.partitions": 512,
    "spark.executor.cores": 4,
    "spark.driver.memory": "4g",
    "spark.executor.memory": "8g",
    "spark.executor.memoryOverhead": "1g",
    "spark.jars.packages": "org.apache.spark:spark-avro_2.12:3.1.2",
    "spark.sql.execution.arrow.pyspark.enabled": "true",
}

def create_yarn_spark_session(
        app_id="sparkling",
        extra_config={},
        gitlab_project="repos/research/research-common",
        version=None,
        dev=False,
        conda_env=None):
    """
    Creates a spark context.

    While there are basic default spark configuration options, this method makes no assumptions/changes
    about the config except for the python configuration used for the yarn workers.

    :param app_id:
        Spark session name that will show up on yarn.wikimedia.org

    :param extra_config:
        Configuration options that are passed to spark. They override the basic defaults
        defined in `default_spark_config`

    :param gitlab_project:
        A name of the gitlab project with CI that generates conda environment packages. The most
        recently built package from Gitlab CI is used.

    :param version:
        The version, can be a semantic version like 0.1.1 or a
        branch name.

    :param dev:
        If True, use the dev release. If no version is provided, use the
        recent conda env built for a dev release. If version is a branch name,
        don't set dev=True as it applies only to semantic versions.

    :param conda_env:
        (Optional) URI to a conda environment. Use to use a different conda env than latest package
        from a gitlab project. The URI can be a url, a local conda env or hdfs path.

    :return: yarn spark session
    """

    spark_config = {}
    spark_config.update(default_spark_config)
    spark_config.update(extra_config)

    # setup up python env configuration
    spark_config["spark.pyspark.driver.python"] = "python"
    spark_config["spark.pyspark.python"] = "./env/bin/python"

    def add_as_archive(archive):
        aliased = f"{archive}#env"
        if "spark.yarn.dist.archives" not in spark_config:
            spark_config["spark.yarn.dist.archives"] = aliased
        else:
            spark_config["spark.yarn.dist.archives"] += f",{aliased}"

    if conda_env:
        add_as_archive(conda_env)
    else:
        add_as_archive(latest_package_file(gitlab_project, version, dev))

    # setting required env variables
    os.environ["SPARK_HOME"] = SPARK_HOME
    # spark-submit also requires the python env variables to be set
    os.environ["PYSPARK_DRIVER_PYTHON"] = spark_config["spark.pyspark.driver.python"]
    os.environ["PYSPARK_PYTHON"] = spark_config["spark.pyspark.python"]

    builder = SparkSession.builder
    for k, v in spark_config.items():
        builder.config(k, v)
    return (builder
            .master('yarn')
            .appName(app_id)
            .getOrCreate())
