import re

import gitlab

# discuss whether something like this will be in the artifact abstraction
def latest_package_file(
        project_path: str,
        version: str = None,
        dev: bool = False,
        package_files_index: int = -1) -> str:
    """
    Returns the URL of the latest package file built by gitlab CI
    for a project. Example: most recent conda environment for
    a python project.

    :param project_path:
        Name of the Wikimedia gitlab project, e.g.
        'repos/research/example-research-project'
    :param version:
        The version, can be a semantic version like 0.1.1 or a
        branch name for which a manual conda env CI pipeline was
        triggered.
    :param dev:
        If True, use the dev release. If no version is provided, use the
        recent conda env built for a dev release. If version is a branch name,
        don't set dev=True as it applies only to semantic versions.
    :param package_files_index:
        Index of the time-sorted (asc) package files for the package defined
        by project/version/dev. Defaults to most recently built package-file,
        e.g. use -2 to get the file built in the previous ci pipeline run.

    :return:
        URL to a downloadable file
    """
    semantic_version_re = re.compile('(?P<major>\d+)\.(?P<minor>\d+)\.(?P<patch>\d+)(\.(?P<release>[a-z0-9]+))?')
    if version and dev:
        assert semantic_version_re.match(version), f"for dev releases, the version must be semantic (provided: {version})"

    gl = gitlab.Gitlab('https://gitlab.wikimedia.org')
    project = gl.projects.get(project_path)

    # sort packages by age of versions
    sorted_packages = sorted(project.packages.list(),key=lambda p: p.created_at,reverse=True)

    # choose the most recent package given the provided configuration
    package = None
    for p in sorted_packages:
        semantic_version = semantic_version_re.match(p.version)
        if semantic_version:
            # XOR
            # if dev=False, p.version should not contain 'dev'
            # if dev=True, p.version should contain 'dev'
            if ('dev' in p.version) ^ (not dev):
                if version:
                    if version in p.version:
                        package = p
                        break
                else:
                    package = p
                    break

        else:
            # a branch release
            if version == p.version:
                package = p
                break

    if not package:
        raise ValueError(f"no package found for project:{project_path}, version:{version}, dev release:{dev}")

    # get the package file built by CI
    package_files = sorted(package.package_files.list(),key=lambda p: p.created_at)

    if package_files_index == -1:
        # generally the latest package file is used, the generic path
        # points to the most recently built package version.
        # file name is the same for all packages built for this package
        file_name = package_files[package_files_index].file_name
        return f"https://gitlab.wikimedia.org/api/v4/projects/{project.id}/packages/generic/{package.name}/{package.version}/{file_name}"
    else:
        # use a different package file for the provided package version,
        # e.g. the get the previously built package for testing
        # the id of the built package can be referenced directly
        package_id = package_files[package_files_index].id
        return f"https://gitlab.wikimedia.org/{project.path_with_namespace}/-/package_files/{package_id}/download"
