import base64
from datetime import timedelta
import hashlib
from pyspark.sql import Row
import pyspark.sql.functions as F
import pyspark.sql.types as T
import pyspark
import requests

swift_host = "https://ms-fe.svc.eqiad.wmnet"
max_retries = 5
timeout = 15

def decode_b64(b64_string):
    """
    Decode the base64 encoded swift file object bytes into a byte array

    Bytes are not a supported type by spark, the file object bytes
    retrieved from swift is stored as base64 encoded string in the
    spark dataframe. The base64 string should only be decoded when
    the underlying bytes are used, e.g. in a udf that parses the
    bytes of an image.

    :param b64_string: base64 encoded string
    :return: byte array
    """
    return base64.b64decode(b64_string)

def get_file_bytes(project, file_name, thumb_size):
    md5 = hashlib.md5(file_name.encode('utf-8')).hexdigest()
    shard = f"{md5[0]}/{md5[:2]}"
    if thumb_size:
        uri = f"{swift_host}/wikipedia/{project}/thumb/{shard}/{file_name}/{thumb_size}-{file_name}"
    else:
        uri = f"{swift_host}/wikipedia/{project}/{shard}/{file_name}"

    # res = requests.get(uri, verify=True, timeout=timeout)
    res = requests.get(uri, stream=True, verify=True, timeout=timeout)
    res.raise_for_status()
    max_file_size=10e6 # ~10mb
    res_bytes = b''
    size = 0
    for chunk in res.iter_content(1024):
        res_bytes = res_bytes + chunk
        size += len(chunk)
        if size > max_file_size:
            raise ValueError(f'file larger than {max_file_size} bytes')
    return res_bytes

@F.udf(returnType=T.ArrayType(T.StringType()))
def download_file(file_name, project, thumb_size):
    #requests's built in retries causes some dependency issue on the worker, doing retries manually
    error=None
    for retry_count in range(max_retries):
        try:
            file_bytes = get_file_bytes(project, file_name, thumb_size)
            return (base64.b64encode(file_bytes).decode('utf-8'), None)
        except requests.exceptions.HTTPError as e:
            print(f"try {retry_count}, swift download error for file {file_name}. Exception: {e}")
            error=str(e)
            if e.response.status_code==404:
                # no need to retry
                break
        except ValueError as e:
            print(f"file {file_name} : {str(e)}")
            error=str(e)
            break
        except Exception as e:
            print(f"try {retry_count}, swift download error for file {file_name}. Exception: {e}")
            error=str(e)
    return (None, error)

def download_files(input_df, partitions, thumb_size=None):
    """
    `input_df` is expected to have a `file_name` and a `project` column.
    `thumb_size` int used for download image file types (e.g. 300)

    returns two dataframes
        `input` dataframe with a `file_bytes_b64` column containing the raw file bytes as base64 encoded string
        `input` dataframe with a `download_error` column containing the errors for each file that wasn't downloaded
    """

    input_df = input_df.repartition(partitions) if partitions is not None else input_df

    thumb_col = F.lit(f"{thumb_size}px") if thumb_size else F.lit(None)
    input_df = input_df.withColumn('thumbnail_size', thumb_col)

    attempted_file_bytes = (input_df
        .withColumn('attempted_files_bytes', download_file('file_name', 'project', 'thumbnail_size'))
        .drop('thumbnail_size')
        .persist(pyspark.StorageLevel.DISK_ONLY))

    download_errors = (attempted_file_bytes
        .withColumn('download_error', F.col('attempted_files_bytes').getItem(1))
        .where(F.col('download_error').isNotNull())
        .drop('attempted_files_bytes'))

    with_file_bytes = (attempted_file_bytes
        .where(F.col('attempted_files_bytes').getItem(0).isNotNull())
        .withColumn('file_bytes_b64', F.col('attempted_files_bytes').getItem(0))
        # .withColumn('image', parse_image(F.col('attempted_files_bytes').getItem(0)))
        .drop('attempted_files_bytes'))

    return (with_file_bytes, download_errors)

def append_image_bytes(
    input_df,
    mb_per_file_object=None,
    output_dir=None,
    partition_size_mb=200,
    thumb_size=None,
    swift_download_errors_dir=None):
    """
    Advisory: The number of threads querying swift will depend on the configuration
    of the spark session used to create the `input_df`. Beware! For example, a
    spark session with 10 executors and 2 cores per executor will result in 20 threads
    querying swift (assuming the dataframe has 20+ partitions).

    The number of queries per second (qps) to swift also depends on the size of the
    file objects downloaded. Aim for a load of <100qps to swift. The load on
    the swift cluster should be monitored on the 'swift' dashboard on https://grafana.wikimedia.org/.

    Note that if a `output_dir` or `swift_download_errors_dir` are provided, the pipeline
    is executed and the results written to the specified directories. However, if no output
    directory is specified, the returned dataframes are an execution plan only.

    :param input_df: DataFrame is required to have columns `file_name` and `project`.
    :param mb_per_file_object: if passed, the expected average swift file object size is used to estimate the number of
        partitions required to generate partitions of size `partition_size_mb`
    :param output_dir: if passed the directory where avro files are written to
    :param thumb_size: int, can be used for download thumbnails of image file types
    :param swift_download_errors_dir: if passed, the swift download errors are stored on disk
    :returns: Tuple of dataframes (`files`, `errors`).
        `files` is the input dataframe with a `file_bytes_b64` column containing the
        raw file object bytes as base64 encoded string.
        `errors` is the input dataframe with a `download_error` column containing the error
        for each file that wasn't downloaded successfully
    """
    num_partitions=None
    if mb_per_file_object:
        print('counting total number of files for file partitioning')
        num_files = input_df.count()
        num_partitions = estimate_partitions(num_files=num_files,mb_per_file=mb_per_file_object,file_size_mb=partition_size_mb)

    files, errors = download_files(
        input_df=input_df,
        partitions=num_partitions,
        thumb_size=thumb_size)

    if output_dir:
        print(f"downloading files into {files.rdd.getNumPartitions()} partitions and writing them to {output_dir}")
        files.write.format("avro").mode("overwrite").save(output_dir)

    if swift_download_errors_dir:
        print(f"writing errors from swift to {swift_download_errors_dir}")
        errors.write.format("avro").mode("overwrite").save(swift_download_errors_dir)

    return files, errors

def estimate_partitions(num_files: int, mb_per_file: float, file_size_mb: float) -> int:
    """
    Helper method to estimate the number of partitions to create
    output files of an approximate size `file_size_mb`

    :param num_files: total number of files to download from swift
    :param mb_per_file: average size in megabytes of files to download
    :param file_size_mb: desired output file size in megabytes
    :returns: estimated number of partitions required
    """
    num_partitions = int(num_files*mb_per_file/file_size_mb)
    total_mb = num_files*mb_per_file
    print(f"""{num_partitions} partitions are required to store {num_files}
    swift file objects (~{mb_per_file}mb) in output files of size
    {file_size_mb}mb. Expected total size {total_mb/1e3}GB.
    """)
    return max(num_partitions,1)

